# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 21:19:04 2021

@author: AGLUR
"""



from xml.dom import minidom

import matplotlib.pyplot as plt
import scipy.linalg as la
import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy

from vtk.numpy_interface import dataset_adapter as dsa

import pandas as pd
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['pdf.fonttype'] = 42 #to export editable text to illustrator
degsign=u"\u2103"

# Function to change string in file (necessary to modify .pvd file fromParaview/OGS)
def ogs6_prj_timestep_update(filename, t_ini, t_fin):
        txt=""
        with open(filename, 'r') as filein:
             for line in filein:
                if "<t_initial>" in line:
                    line="                    <t_initial>"+str(t_ini)+"</t_initial> \n"
                elif "<t_end>" in line:
                    line="                    <t_end>"+str(t_fin)+"</t_end> \n"
                txt=txt+line
             return txt  


# Function to change string in file (necessary to modify .pvd file fromParaview/OGS)
def inplace_change(filename, old_string, new_string):
        s=open(filename).read()
        if old_string in s:
                print('Changing "{old_string}" to "{new_string}"'.format(**locals()))
                s=s.replace(old_string, new_string)
                f=open(filename, 'w')
                f.write(s)
                f.flush()
                f.close()
        else:
                print('No occurances of "{old_string}" found.'.format(**locals()))
    
def read_vtufiles(pvd_filename):          
        inplace_change(filename,'/>','></DataSet>')
        xmldoc = minidom.parse(filename)
        itemlist = xmldoc.getElementsByTagName('DataSet')
#         print(len(itemlist))
        #print(itemlist[0].attributes['file'].value)
        filenames=[]
        vtu_times=[]
        for s in itemlist:
                filenames.append(s.attributes['file'].value)
                vtu_times.append(s.attributes['timestep'].value)
        inplace_change(filename,'></DataSet>','/>')    
        return filenames,vtu_times

def out_arr( outfile,list):
 filename=outfile
 f = open(filename, 'w')
 f.write("\n".join(list))
 f.close()
 return;
def out_str( outfile,list):
 filename=outfile
 f = open(filename, 'w')
 f.write(list)
 f.close()
 return;

def MohrCoulomb2D(sxx,syy,sxy,nodes, tanp,cosp):
    # defined as in Abaqus, see 
    # file:///C:/Users/aglur/Documents/DGR_hlw&co/Mohr-Coulomb%20model.pdf
    # yield surface F
    # F = Rmc*q - p*tan(frict) - c =0
    p= [[] for oid in range(len(nodes))]
    q= [[] for oid in range(len(nodes))]
    r= [[] for oid in range(len(nodes))]
    Rmc= [[] for oid in range(len(nodes))]
    for i in range(len(nodes)):
        for j in range(len(sxx[i])):
            sxx_t=sxx[i][j]
            syy_t=syy[i][j]
            sxy_t=sxy[i][j]
            norm_t=(sxx_t+syy_t)/2
            dev_t=(sxx_t-syy_t)+sxy_t
            sig_t=(np.array([[sxx[i][j],sxy[i][j]],[sxy[i][j],syy[i][j]]])) 
            eigvals, eigvecs = la.eig(sig_t)
            I1=eigvals.real[0]+eigvals.real[1]
            I2=eigvals.real[0]*eigvals.real[1]
            I3=eigvals.real[0]*eigvals.real[1]
            p_t=(1/2)*(eigvals.real[0]+eigvals.real[1]) #1st inv, equivalent pressure stress
            q_t=np.sqrt(1/2 * np.power(eigvals.real[0]-eigvals.real[1],2))# Mises equivalent stress
            r_t= 2/27*np.power(I1,3) - (1/3) * (I1*I2) +I3#np.sqrt((9/2) * eigvals.real[0]*eigvals.real[1])#np.power(9/2S . S : S , 1/3) #3rdinvariant of deviatoric stress
            Theta = np.power(r_t/q_t,3) # deviatoric polar angle  
            Rmc_t= 1/(np.sqrt(3)*cosp)*np.sin(Theta+np.pi/3)+1/3 * (np.cos(Theta+np.pi/3))*tanp#Mohr coulomb deviaoric stress 
            p[i].append(p_t)
            q[i].append(q_t)
            r[i].append(r_t)
            Rmc[i].append(Rmc_t)
        
 
    return p,q, r, Rmc;


def MohrCoulomb2D_simple(sxx,syy,sxy,nodes, tanp,cosp):
    # defined as in Abaqus, see 
    # file:///C:/Users/aglur/Documents/DGR_hlw&co/Mohr-Coulomb%20model.pdf
    # yield surface F
    # F = Rmc*q - p*tan(frict) - c =0
    p= [[] for oid in range(len(nodes))]
    q= [[] for oid in range(len(nodes))]
    r= [[] for oid in range(len(nodes))]
    Rmc= [[] for oid in range(len(nodes))]
    for i in range(len(nodes)):
        for j in range(len(sxx[i])):
            sxx_t=sxx[i][j]
            syy_t=syy[i][j]
            sxy_t=sxy[i][j]
            norm_t=(sxx_t+syy_t)/2
            dev_t=(sxx_t-syy_t)+sxy_t
            sig_t=(np.array([[sxx[i][j],sxy[i][j]],[sxy[i][j],syy[i][j]]])) 
            eigvals, eigvecs = la.eig(sig_t)
            norm=0.5*(eigvals.real[0]+eigvals.real[1])
            shear=0.5*(eigvals.real[0]-eigvals.real[1])
            p[i].append(norm)
            q[i].append(shear)
        
 
    return p,q, r, Rmc;
#%%
def rel_displ(nodes,displ) : # compute relative displ of a node wrt to the other
      dx=[a[0]-b[0] for a,b in zip(displ[nodes_list.index(nodes[0])][:],displ[nodes_list.index(nodes[1])][:])]
      dy=[a[1]-b[1] for a,b in zip(displ[nodes_list.index(nodes[0])][:],displ[nodes_list.index(nodes[1])][:])]
      dz=[a[2]-b[2] for a,b in zip(displ[nodes_list.index(nodes[0])][:],displ[nodes_list.index(nodes[1])][:])]
      rel_d=[dx,dy,dz]
      return rel_d
#%%
"""
#all nodes

"""
nodes_list=[ 9792, 4766, 32621, 814, 1694, 12529,12181]
location=[str(a) for a in nodes_list]
linestyles=['-','-','--','-','-','-','--','-','-','-','--','-','-','-','--','-','-','-','--','-']
T=[]
sig=[]
displ=[]
folder="PW-10perm_rheol"
#filename=folder+'//FM-HM_2d_quick-rupt.pvd'
filename="D2023_FM_THM.pvd"
folder_plot="plot"
  

# zeit=[0,3.1536001E+08,4.7315E+08,6.3094E+08,7.8872E+08,9.4651E+08,1.1043E+09,1.2621E+09,1.4199E+09,1.5777E+09,1.7355E+09,1.8932E+09,2.0510E+09,2.2088E+09,2.3666E+09,2.5244E+09,2.6822E+09,2.8400E+09,2.9978E+09,3.1555E+09,3.3133E+09,3.4711E+09,3.6289E+09,3.7867E+09,3.9445E+09,4.1023E+09,4.2601E+09,4.4178E+09,4.5756E+09,4.7334E+09,4.8912E+09,5.0490E+09,5.2068E+09,5.3646E+09,5.5224E+09,5.6802E+09,5.8379E+09,5.9957E+09,6.1535E+09,6.3113E+09,6.4691E+09,6.6269E+09,6.7847E+09,6.9425E+09,7.1002E+09,7.2580E+09,7.4158E+09,7.5736E+09,7.7314E+09,7.8892E+09,8.0470E+09,8.2048E+09,8.3625E+09,8.5203E+09,8.6781E+09,8.8359E+09,8.9937E+09,9.1515E+09,9.3093E+09,9.4671E+09,9.6249E+09,9.7826E+09,9.9404E+09,1.0098E+10,1.0256E+10,1.0414E+10,1.0572E+10,1.0729E+10,1.0887E+10,1.1045E+10,1.1203E+10,1.1361E+10,1.1518E+10,1.1676E+10,1.1834E+10,1.1992E+10,1.2149E+10,1.2307E+10,1.2465E+10,1.2623E+10,1.2781E+10,1.2938E+10,1.3096E+10,1.3254E+10,1.3412E+10,1.3570E+10,1.3727E+10,1.3885E+10,1.4043E+10,1.4201E+10,1.4358E+10,1.4516E+10,1.4674E+10,1.4832E+10,1.4990E+10,1.5147E+10,1.5305E+10,1.5463E+10,1.5621E+10,1.5779E+10,1.5936E+10,1.6094E+10,1.6252E+10,1.6410E+10,1.6568E+10,1.6725E+10,1.6883E+10,1.7041E+10,1.7199E+10,1.7356E+10,1.7514E+10,1.7672E+10,1.7830E+10,1.7988E+10,1.8145E+10,1.8303E+10,1.8461E+10,1.8619E+10,1.8777E+10,1.8934E+10,1.9092E+10,1.9250E+10,1.9408E+10,1.9565E+10,1.9723E+10,1.9881E+10,2.0039E+10,2.0197E+10,2.0354E+10,2.0512E+10,2.0670E+10,2.0828E+10,2.0986E+10,2.1143E+10,2.1301E+10,2.1459E+10,2.1617E+10,2.1775E+10,2.1932E+10,2.2090E+10,2.2248E+10,2.2406E+10,2.2563E+10,2.2721E+10,2.2879E+10,2.3037E+10,2.3195E+10,2.3352E+10,2.3510E+10,2.3668E+10,2.3826E+10,2.3984E+10,2.4141E+10,2.4299E+10,2.4457E+10,2.4615E+10,2.4773E+10,2.4930E+10,2.5088E+10,2.5246E+10,2.5404E+10,2.5561E+10,2.5719E+10,2.5877E+10,2.6035E+10,2.6193E+10,2.6350E+10,2.6508E+10,2.6666E+10,2.6824E+10,2.6982E+10,2.7139E+10,2.7297E+10,2.7455E+10,2.7613E+10,2.7770E+10,2.7928E+10,2.8086E+10,2.8244E+10,2.8402E+10,2.8559E+10,2.8717E+10,2.8875E+10,2.9033E+10,2.9191E+10,2.9348E+10,2.9506E+10,2.9664E+10,2.9822E+10,2.9980E+10,1.5810E+11,3.1589E+11,1.5782E+12,3.1561E+12,1.5779E+13,3.1558E+13]
# zeit_wq=np.array(zeit)/(86400*365.25)
# wq_hlw=[1.63E+02,1.63E+02,1.48E+02,1.35E+02,1.25E+02,1.16E+02,1.09E+02,1.02E+02,9.69E+01,9.20E+01,8.77E+01,8.38E+01,8.02E+01,7.70E+01,7.41E+01,7.14E+01,6.89E+01,6.67E+01,6.45E+01,6.26E+01,6.07E+01,5.90E+01,5.74E+01,5.59E+01,5.45E+01,5.31E+01,5.19E+01,5.07E+01,4.95E+01,4.85E+01,4.74E+01,4.64E+01,4.55E+01,4.46E+01,4.38E+01,4.29E+01,4.22E+01,4.14E+01,4.07E+01,4.00E+01,3.93E+01,3.87E+01,3.81E+01,3.75E+01,3.69E+01,3.63E+01,3.58E+01,3.53E+01,3.48E+01,3.43E+01,3.38E+01,3.34E+01,3.29E+01,3.25E+01,3.21E+01,3.17E+01,3.13E+01,3.09E+01,3.05E+01,3.02E+01,2.98E+01,2.95E+01,2.91E+01,2.88E+01,2.85E+01,2.82E+01,2.79E+01,2.76E+01,2.73E+01,2.70E+01,2.68E+01,2.65E+01,2.62E+01,2.60E+01,2.57E+01,2.55E+01,2.52E+01,2.50E+01,2.48E+01,2.45E+01,2.43E+01,2.41E+01,2.39E+01,2.37E+01,2.35E+01,2.33E+01,2.31E+01,2.29E+01,2.27E+01,2.25E+01,2.23E+01,2.22E+01,2.20E+01,2.18E+01,2.17E+01,2.15E+01,2.13E+01,2.12E+01,2.10E+01,2.09E+01,2.07E+01,2.06E+01,2.04E+01,2.03E+01,2.01E+01,2.00E+01,1.98E+01,1.97E+01,1.96E+01,1.94E+01,1.93E+01,1.92E+01,1.91E+01,1.89E+01,1.88E+01,1.87E+01,1.86E+01,1.85E+01,1.83E+01,1.82E+01,1.81E+01,1.80E+01,1.79E+01,1.78E+01,1.77E+01,1.76E+01,1.75E+01,1.74E+01,1.73E+01,1.72E+01,1.71E+01,1.70E+01,1.69E+01,1.68E+01,1.67E+01,1.66E+01,1.65E+01,1.64E+01,1.63E+01,1.63E+01,1.62E+01,1.61E+01,1.60E+01,1.59E+01,1.58E+01,1.58E+01,1.57E+01,1.56E+01,1.55E+01,1.54E+01,1.54E+01,1.53E+01,1.52E+01,1.51E+01,1.51E+01,1.50E+01,1.49E+01,1.49E+01,1.48E+01,1.47E+01,1.46E+01,1.46E+01,1.45E+01,1.44E+01,1.44E+01,1.43E+01,1.42E+01,1.42E+01,1.41E+01,1.41E+01,1.40E+01,1.39E+01,1.39E+01,1.38E+01,1.38E+01,1.37E+01,1.36E+01,1.36E+01,1.35E+01,1.35E+01,1.34E+01,1.34E+01,1.33E+01,1.32E+01,1.32E+01,1.31E+01,1.31E+01,1.30E+01,1.30E+01,1.29E+01,3.86E+00,2.78E+00,5.87E-01,2.47E-01,1.28E-01,9.12E-02]

# fig = plt.figure(figsize=[7.8, 4.50])
# ax = fig.add_subplot(111)
# print("Plot Heat source")
# im2=ax.semilogx(zeit_wq,wq_hlw, lw=3, ls='--')
# #ax.plot(t,0.2*ty, 'c--')
# ax.axis([0.1, 1e5, 0, 250])
# fig.suptitle("Heat source evolution",fontsize = 24)
# ax.set_xlabel('Zeit (a)', fontsize=20)
# ax.set_ylabel('W/(meter of tunnel)', fontsize=20)
# ax.tick_params(axis='x', labelsize=15)
# ax.tick_params(axis='y', labelsize=15)
# plt.grid()
# plt.legend()
# plt.tight_layout()

# fig.savefig(folder_plot+"//A2_heatsource_pro_m_tunnel_v2.png", dpi=300)

 
vtu_list,vtu_t=(read_vtufiles(filename))


length=len(nodes_list)
time_node= [[] for oid in range(length)]
temperature= [[] for oid in range(length)]
pressure= [[] for oid in range(length)]
sig= [[] for oid in range(length)]
displ= [[] for oid in range(length)]
sigxx= [[] for oid in range(length)]
sigyy= [[] for oid in range(length)]
sigzz= [[] for oid in range(length)]
sigxy= [[] for oid in range(length)]
sigyz= [[] for oid in range(length)]
displx= [[] for oid in range(length)]
disply= [[] for oid in range(length)]
displz= [[] for oid in range(length)]
x= [[] for oid in range(length)]
y= [[] for oid in range(length)]
ratio= [[] for oid in range(length)]
time_color= [[] for oid in range(length)]
#time= [[] for oid in range(length)]
colorst= [[] for oid in range(length)]
strength= [[] for oid in range(length)]
steps=[]
time= [[] for oid in range(length)]
max_sig=0  
max_T=0      
#%%
print('Reading')
for ii in range(len(vtu_list)):    
    vtu_file=vtu_list[ii]
    reader = vtk.vtkXMLUnstructuredGridReader()
    # reader.SetFileName(folder+'//'+vtu_file)
    reader.SetFileName(vtu_file)
    reader.Update()
    nodes_vtk_array= reader.GetOutput().GetPoints().GetData()
    nodes_nummpy_array = vtk_to_numpy(nodes_vtk_array)
    displ_vtu= vtk_to_numpy(reader.GetOutput().GetPointData().GetArray("displacement"))
    sig_vtu= vtk_to_numpy(reader.GetOutput().GetPointData().GetArray("sigma")) 
    T_vtu= vtk_to_numpy(reader.GetOutput().GetPointData().GetArray("pressure_interpolated"))   
    print("Time step "+str(ii+1)+" of "+str(len(vtu_list)))
    for jj in range(len(nodes_list[:])):
           print("\b"*8,end='')
           print("File "+str(jj+1)+" of "+str(len(nodes_list)),  end = '')
           node_id=nodes_list[jj]
           #sorted and ready to accomdoate values    
           #if ii==0:
                    #location[jj]+=(" x: "+str(nodes_nummpy_array[:,0][node_id])+", z: "+str(nodes_nummpy_array[:,1][node_id]))
           #print(node_id)
           x[jj].append(nodes_nummpy_array[:,0][node_id])
           y[jj].append(nodes_nummpy_array[:,1][node_id])
           sig[jj].append(sig_vtu[node_id]) # pick stress component 0 x 1 y 2 z 3 xy
           sigxx[jj].append(-sig_vtu[node_id][0]/1.e6) # pick stress component 0 x 1 y 2 z 3 xy
           sigyy[jj].append(-sig_vtu[node_id][1]/1.e6) # pick stress component 0 x 1 y 2 z 3 xy
           sigxy[jj].append(-sig_vtu[node_id][3]/1.e6) # pick stress component 0 x 1 y 2 z 3 xy
           sigzz[jj].append(-sig_vtu[node_id][2]/1.e6) # pick stress component 0 x 1 y 2 z 3 xy
           sigyz[jj].append(-sig_vtu[node_id][5]/1.e6) # pick stress component 0 x 1 y 2 z 3 xy
           displ[jj].append(displ_vtu[node_id])
           displx[jj].append(displ_vtu[node_id][0])  # pick stress component 0 x 1 y
           disply[jj].append(displ_vtu[node_id][1])  # pick stress component 0 x 1 y           
           displz[jj].append(displ_vtu[node_id][2])  # pick stress component 0 x 1 y           
           #temperature[jj].append(T_vtu[node_id]-273.15)
           pressure[jj].append(T_vtu[node_id])
           #if float(vtu_t[ii])<1:
           # vtu_t[ii]="1.0"
           time_node[jj].append(float(vtu_t[ii]))#/86400/365.25)
           # if T_vtu[node_id]>max_T:
           #     max_T=T_vtu[node_id]
           #     max_time=float(vtu_t[ii])#/86400/365.25
           time.append(float(vtu_t[ii]))#/86400/365.25)
           steps.append(float(vtu_t[ii])/float(vtu_t[-1]))
    print()
           
    

print('Plotting')
#%%
####
# Mohr Coulomb param
####
#
#frict=30 #degrees
#coh=2.5 #MPa
#
#tanp=np.tan(np.deg2rad(frict))
#cosp=np.cos(np.deg2rad(frict))
#p, q, r ,Rmc = MohrCoulomb2D_simple(sigxx,sigyy,sigxy, nodes_list, tanp,cosp)
####
fig = plt.figure(figsize=[7.8, 4.50])
ax = fig.add_subplot(111)
for i in range(len(nodes_list[:])):
 print("File "+str(i+1)+" of "+str(len(nodes_list)))
 t = np.arange(0., 16, 1e-1)
 ty = np.arange(0., 16, 1e-1)
 # im= ax.scatter(p[i][:-1], q[i][:-1], c=time_node[i][:-1], s=150, cmap=plt.cm.viridis, norm=mpl.colors.LogNorm(), edgecolors='None', alpha=0.75)
 # im3=ax.quiver(pos_x[10:-1], pos_y[10:-1], u[10:-1]/norm, v[10:-1]/norm, width=0.005, angles="xy", zorder=2, alpha=0.5, pivot="mid")
 im2=ax.plot(time_node[i],pressure[i], ls=linestyles[i], lw=3, alpha=0.66, label=location[i])
 #ax.plot(t,0.2*ty, 'c--')
#ax.axis([0.1, 1e5, 24.9, 50.5])
fig.suptitle('Pressure Evolution',fontsize = 24)
ax.set_xlabel('Time (s))', fontsize=20)
ax.set_ylabel('Pressure (Pa)', fontsize=20)# ['+degsign+']', fontsize=20)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
plt.grid()
plt.legend()
plt.tight_layout()
fig.savefig("3d_"+"_pressures.png", dpi=300)

#plt.close('all')


#%%

#compute delta displacement between anchors
nodes_ap=(9792,  32621)
ds=rel_displ(nodes_ap,displ) #nodes_list, displacement array , output ds=[dx,dy,dz]
#%%
fig = plt.figure(figsize=[7.8, 4.50])
ax = fig.add_subplot(111)
im2=ax.plot(time_node[0],ds[1], lw=3, alpha=0.66, label='dy rel displ nodes %d %d' %(nodes_ap[0],nodes_ap[1]))
im2=ax.plot(time_node[0],ds[2], lw=3, alpha=0.66, label='dz rel displ nodes %d %d' %(nodes_ap[0],nodes_ap[1]))
#ax.plot(t,0.2*ty, 'c--')
#ax.axis([0.1, 1e5, 24.9, 50.5])
fig.suptitle('displ (m)',fontsize = 24)
ax.set_xlabel('Time (s)', fontsize=20)
ax.set_ylabel('Pressure [MPa]', fontsize=20) ##['+degsign+']', fontsize=20)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
plt.grid()
plt.legend()
plt.tight_layout()
plt.show()
fig.savefig("3d_"+"ap_displacements.png", dpi=300)
#plt.close('all')


####
# Mohr Coulomb param
####

frict=42#degrees
coh=0 #MPa

tanp=np.tan(np.deg2rad(frict))
cosp=np.cos(np.deg2rad(frict))
p, q, r ,Rmc = MohrCoulomb2D_simple(sigyy,sigzz,sigyz, nodes_list, tanp,cosp)
####
fig = plt.figure(figsize=[7.8, 4.50])
ax = fig.add_subplot(111)
for i in range(len(nodes_list[:])):
 print("File "+str(i+1)+" of "+str(len(nodes_list)))
 t = np.arange(0., 16, 1e-1)
 ty = np.arange(0., 16, 1e-1)
 im= ax.scatter(p[i][:-1], q[i][:-1], c=time_node[i][:-1], s=150, cmap=plt.cm.viridis, norm=mpl.colors.LogNorm(), edgecolors='None', alpha=0.75)
 #im3=ax.quiver(pos_x[10:-1], pos_y[10:-1], u[10:-1]/norm, v[10:-1]/norm, width=0.005, angles="xy", zorder=2, alpha=0.5, pivot="mid")
 #im2=ax.plot(time_node[i],pressure[i], ls=linestyles[i], lw=3, alpha=0.66, label=location[i])
 ax.plot(t,tanp*ty+coh, 'c--')
 ax.plot(t,-tanp*ty-coh, 'c--')
#ax.axis([0.1, 1e5, 24.9, 50.5])
fig.suptitle('Stress Evolution along plane of weakness',fontsize = 24)
ax.set_xlabel('Normal stress (Pa)', fontsize=20)
ax.set_ylabel('Shear stress (Pa)', fontsize=20)# ['+degsign+']', fontsize=20)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
plt.grid()
plt.legend()
plt.tight_layout()
fig.savefig("3d_"+"_MC.png", dpi=300)
